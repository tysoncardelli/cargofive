
const { Sequelize } = require('sequelize');
import Server from "../models/server";
import request from "supertest";

const server = new Server()
const req = request(server.app);

let sequelize: any;

beforeAll(async () => {
    // Sequelize configuration for testing
    sequelize = new Sequelize(process.env.DB, process.env.U, process.env.PASSWORD, {
        host: process.env.HOST,
        dialect: 'mysql',
    })

    // Test connection
    await sequelize.authenticate();
});

afterAll(async () => {
    // Closing connection
    await sequelize.close();
});

describe('getAll', () => {

    it("should return all surcharges", async () => {
        const res = await req.get("/api/surcharges");
        expect(res.status).toBe(200);
    });

    it("should return all carriers", async () => {
        const res = await req.get("/api/carriers");
        expect(res.status).toBe(200);
    });

    it("should return all standard surcharges", async () => {
        const res = await req.get("/api/standard_surcharges");
        expect(res.status).toBe(200);
    });

    it("should return all rates", async () => {
        const res = await req.get("/api/rates");
        expect(res.status).toBe(200);
    });

    it("should return all calculation types", async () => {
        const res = await req.get("/api/calculation_types");
        expect(res.status).toBe(200);
    });
});

describe('testing CRUD', () => {

    it("should return all surcharges", async () => {
        const res = await req.get("/api/surcharges");
        expect(res.status).toBe(200);
    });

    it("should save a surcharge", async () => {
        const requestBody = {
            "id": 100,
            "name": "test",
            "apply_to": "freight",
            "calculation_type_id": 1
        }
        const res = (await req.post("/api/surcharges").send(requestBody));
        expect(res.status).toBe(200);
    });

    it("should get one a surcharge", async () => {
        const res = (await req.get("/api/surcharges/100"));
        expect(res.status).toBe(200);
    });

    it("should put a surcharge", async () => {
        const requestBody = {
            "name": "test1",
        }
        const res = (await req.put("/api/surcharges/100").send(requestBody));
        expect(res.status).toBe(200);
    });

    it("should delete a surcharge", async () => {

        const res = (await req.delete("/api/surcharges/100"));
        expect(res.status).toBe(200);
    });

});

describe('get IA standarize data', () => {

    it("get Standar Data", async () => {
        const res = await req.get("/api/ia/standarize?insert=false");
        expect(res.status).toBe(200);

        expect(Array.isArray(res.body.data)).toBe(true);

        // Verifying structure de data object
        //@ts-ignore
        res.body.data.forEach((item: any) => {
            // Verifyng if every element have the exptect properties 
            expect(item).toHaveProperty("name");
            expect(item).toHaveProperty("keys");
            expect(Array.isArray(item.keys)).toBe(true);
            expect(item).toHaveProperty("apply_to");
            expect(Array.isArray(item.apply_to)).toBe(true);

            // Verifing structure inside apply_to
            item.apply_to.forEach((applyToItem: any) => {
                expect(applyToItem).toHaveProperty("name");
                expect(applyToItem).toHaveProperty("calculation_type_id");
            });
        });

        //Verigyin if rest have the properties error and msg 
        expect(res.body).toHaveProperty("error", false);
        expect(res.body).toHaveProperty("msg", "");


    }, 20000);

});

// });