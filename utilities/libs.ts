import { calculationTypes } from './../models/calculationTypes.model';
var pluralize = require('pluralize')
import { surCharge } from './../models/surcharges.model';
import { carrier } from './../models/carriers.model';
import { rate } from './../models/rates.model';
import { standardSurcharges } from '../models/standarSurcharges.model'



export const getModelName = async (req:any)=>{
    const resource = req.originalUrl.split("/")[2];
    console.log(resource);
    let moduleSplitted = resource.split("?")[0];
    let moduleName = "";
     moduleName = pluralize.singular(moduleSplitted);
    return moduleName
}

export const  doRequest = async (options: any) => {
    return new Promise(function (resolve, reject) {
      var request = require('request');
      request(options, function (error:any, res:any, body:any) {
        if (!error && res.statusCode === 200) {
          resolve(body);
        } else {
          reject(error);
        }
      });
    });

}

export const getModelByName = (modelName: string) => {
    switch (modelName) {
      case 'standard_surcharge':
            return standardSurcharges;
      case 'surcharge':
            return surCharge;
      case 'carrier':
            return carrier;
      case 'rate':
            return rate;
      case 'calculation_type':
            return calculationTypes;
      // Agrega más casos según sea necesario para otros modelos
      default:
        return null;
    }
};

export const createPrompt = async (surcharges:any)=>{
    
    let content = `Agrupa y estandariza  los recargos en la siguiente lista, proporcionando nombres estandarizados para cada uno. Considera la posibilidad de agrupar recargos similares y estandarizar los nombres para facilitar su manejo, nombres con palabras origen y destino se agrupan en el mismo grupo: ${surcharges}. Responde en formato json listo para convertir en una aplicacion de nodejs sin ninguna otra explicación: Ejemplo {"grupo1":["element1, "element2"],"group2":["element1, "element2"],}`

    let messages: any = [
        { "role": "system", "content": content }
    ]

    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.OPENAI_API_KEY
    };
    let body = {
        model: "gpt-3.5-turbo",
        messages: messages,
    }

    let options = {
        url: `https://api.openai.com/v1/chat/completions`,
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body),
    };

    return options;
}

export const apiResponse = async (status:any,data:any, error:any, msg:any)=>{
  return {"statusCode":status, "data":data, "error":error, "msg":msg};
}

