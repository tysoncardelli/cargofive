import {Request, Response} from 'express'
import { apiResponse, getModelByName } from '../utilities/libs';
import { getModelName } from '../utilities/libs';

export const getAll = async(req: Request, res: Response) =>{

    let modelName = await getModelName(req)
    console.log(modelName)
    const Model = await getModelByName(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model)
    
    if (!Model) {
        return res.status(404).json( await apiResponse(400,[],true, 'Model not found'));
    }

    let response = await Model.findAll()
    res.json(await apiResponse(200,response?response: [],false, ""))
}
export const getOne = async(req: Request, res: Response) =>{

  let modelName = await getModelName(req)
  const Model = await getModelByName(modelName);
  console.log("modelName", modelName);
  console.log("model*", Model)
  
  if (!Model) {
       return res.status(404).json(await apiResponse(400,[],true, 'Model not found'));
  }

  let response = await Model.findOne({where: {"id": req.params.id}})
  res.json(await apiResponse(200,response?response: {},false, ""))

}

export const postOne = async(req: Request, res: Response) =>{

  let modelName = await getModelName(req)
  const Model = await getModelByName(modelName);
  console.log("modelName", modelName);
  console.log("model*", Model)
  
  if (!Model) {
      return res.status(404).json({ error: 'Model not found' });
  }
  let response = await Model.create(req.body)
  res.json(await apiResponse(200,response,false, ""))

}

export const updateOne = async(req: Request, res: Response) =>{

  let modelName = await getModelName(req)
  const Model = await getModelByName(modelName);
  console.log("modelName", modelName);
  console.log("model*", Model)
  
  if (!Model) {
      return res.status(404).json({ error: 'Model not found' });
  }
  let response = await Model.update(req.body, {where:{id: req.params.id}})
  let item = await Model.findOne({where: {"id": req.params.id}})

  res.json(await apiResponse(200,item,false, ""))

}

export const deleteOne = async(req: Request, res: Response) =>{

  let modelName = await getModelName(req)
  const Model = await getModelByName(modelName);
  console.log("modelName", modelName);
  console.log("model*", Model)
  
  if (!Model) {
      return res.status(404).json({ error: 'Model not found' });
  }
  let item = await Model.findOne({where: {"id": req.params.id}})
  let response = await Model.destroy({where:{id: req.params.id}})
  res.json(await apiResponse(200,item,false, ""))

}