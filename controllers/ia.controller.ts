import { apiResponse, getModelByName } from '../utilities/libs';
import { createPrompt, doRequest } from '../utilities/libs';
import { Request, Response } from 'express'
import { standardSurcharges } from '../models/standarSurcharges.model'
const XLSX = require('xlsx');
const stringSimilarity = require('string-similarity');


export const standarize = async (req: Request, res: Response) => {

    let surchargeModel = await getModelByName('surcharge')
    let noStandardSurcharges = await surchargeModel.findAll()
    let categories: any = []
    let surcharges = []

    //transforming data to send in to GPT 
    for (let i = 0; i < noStandardSurcharges.length; i++) {
        surcharges.push(noStandardSurcharges[i].dataValues.name)
        categories.push(noStandardSurcharges[i].dataValues)
    }

    // Creating Object Request to GPT     
    let options = await createPrompt(surcharges);
    let response = await doRequest(options)
    //@ts-ignore
    let jsonResponse = JSON.parse(response)

    //Getting Standarize Array Response 
    let standardSurcharges = JSON.parse(jsonResponse.choices[0].message.content);

    const groups = [];

    //Changing  Structure of the Data 
    for (const groupName in standardSurcharges) {
        const keys = standardSurcharges[groupName];
        groups.push({ name: groupName, keys });
    }

    //Triming original array for comparing 
    categories = categories.map((surcharge: any) => {
        surcharge.name = surcharge.name.trim();
        return surcharge;
    });

    //Getting final array to Store 
    const result = groups.map(group => {
        const apply_to: any = [];

        for (const surcharge of categories) {
            if (group.keys.includes(surcharge.name)) {
                // Verifyin if the arary already exists in apply to
                const existingApplyTo = apply_to.find((item: any) => item.name === surcharge.apply_to);

                if (!existingApplyTo) {
                    apply_to.push({
                        name: surcharge.apply_to,
                        calculation_type_id: surcharge.calculation_type_id
                    });
                }
            }
        }

        return {
            name: group.name,
            keys: group.keys,
            apply_to: apply_to
        };
    });

    //Inserting in standard surcharges
    console.log(req.query.insert)
    if(req.query.insert == 'true'){
        let Model = await getModelByName('standard_surcharge')
        for (let i = 0; i < result.length; i++)
            await Model.create(result[i])
    }
    res.json(await apiResponse(200,result,false, ""))
}

export const importData = async (req: Request, res: Response) => {

    let standardData = await standardSurcharges.findAll()

    // transforming array 
    let keyObjectArray = []
    for (let i = 0; i < standardData.length; i++) {
        let object = {
            "id": standardData[i].dataValues.id,
            "keys": []
        }
        //@ts-ignore
        object.keys.push(standardData[i].dataValues.name)
        //@ts-ignore
        object.keys.push(...standardData[i].dataValues.keys)
        keyObjectArray.push(object)
    }

    try {
        // Accessing file data 
        const buffer = (req as any).file.buffer;

        // converting excel content
        const workbook = XLSX.read(buffer, { type: 'buffer' });

        // getting first sheet
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];

        // converting to json array
        const jsonData = XLSX.utils.sheet_to_json(sheet);

        // Aditional logic to get and insert rates
        const mostSimilarIds = await findMostSimilarSurcharge(keyObjectArray, jsonData);

        res.json(await apiResponse(200,jsonData,false, ""));

    } catch (error) {
        console.error(error);
        res.status(500).send('Error processing the file');
    }


}

//@ts-ignore
async function findMostSimilarSurcharge(keys, surcharges) {
    const result: any = [];

    surcharges.forEach(async (surcharge: any) => {
        console.log(surcharge.Surcharge)
        let probabilities: any = []
        //@ts-ignore
        keys.forEach(({ id, keys: keywords }) => {
            let totalSimilarity = 0
            keywords.forEach((keyWord: any) => {
                let similarity = stringSimilarity.compareTwoStrings(surcharge.Surcharge.toLowerCase(), keyWord.toLowerCase());
                totalSimilarity = totalSimilarity + similarity;
            })
            totalSimilarity = totalSimilarity / keywords.length;
            probabilities.push({ "id": id, "probability": totalSimilarity })
        })

        let selected = { "id": 0, "probability": 0 }
        probabilities.forEach((item: any) => {
            if (item.probability > selected.probability) {
                selected.id = item.id
                selected.probability = item.probability
            }
        })
        //console.log(selected)
        let CarrierModel = getModelByName("carrier")
        let rateModel = getModelByName("rate")

        let carrier = await CarrierModel.findOne({ where: { name: surcharge.Carrier } })
        //console.log(carrier)
        let resultObject = {
            "carrier_id": carrier.dataValues.id,
            "surcharge_id": selected.id,
            "amount": surcharge.Amount,
            "currency": surcharge.Currency,
            "apply_to": surcharge["Apply to"]
        }
        await rateModel.create(resultObject)
    })
    return result;
}

