"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const { Sequelize } = require('sequelize');
const server_1 = __importDefault(require("../models/server"));
const supertest_1 = __importDefault(require("supertest"));
const server = new server_1.default();
const req = (0, supertest_1.default)(server.app);
let sequelize;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    // Sequelize configuration for testing
    sequelize = new Sequelize(process.env.DB, process.env.U, process.env.PASSWORD, {
        host: process.env.HOST,
        dialect: 'mysql',
    });
    // Test connection
    yield sequelize.authenticate();
}));
afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
    // Closing connection
    yield sequelize.close();
}));
describe('getAll', () => {
    it("should return all surcharges", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/surcharges");
        expect(res.status).toBe(200);
    }));
    it("should return all carriers", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/carriers");
        expect(res.status).toBe(200);
    }));
    it("should return all standard surcharges", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/standard_surcharges");
        expect(res.status).toBe(200);
    }));
    it("should return all rates", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/rates");
        expect(res.status).toBe(200);
    }));
    it("should return all calculation types", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/calculation_types");
        expect(res.status).toBe(200);
    }));
});
describe('testing CRUD', () => {
    it("should return all surcharges", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/surcharges");
        expect(res.status).toBe(200);
    }));
    it("should save a surcharge", () => __awaiter(void 0, void 0, void 0, function* () {
        const requestBody = {
            "id": 100,
            "name": "test",
            "apply_to": "freight",
            "calculation_type_id": 1
        };
        const res = (yield req.post("/api/surcharges").send(requestBody));
        expect(res.status).toBe(200);
    }));
    it("should get one a surcharge", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = (yield req.get("/api/surcharges/100"));
        expect(res.status).toBe(200);
    }));
    it("should put a surcharge", () => __awaiter(void 0, void 0, void 0, function* () {
        const requestBody = {
            "name": "test1",
        };
        const res = (yield req.put("/api/surcharges/100").send(requestBody));
        expect(res.status).toBe(200);
    }));
    it("should delete a surcharge", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = (yield req.delete("/api/surcharges/100"));
        expect(res.status).toBe(200);
    }));
});
describe('get IA standarize data', () => {
    it("get Standar Data", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/ia/standarize?insert=false");
        expect(res.status).toBe(200);
        expect(Array.isArray(res.body.data)).toBe(true);
        // Verifying structure de data object
        //@ts-ignore
        res.body.data.forEach((item) => {
            // Verifyng if every element have the exptect properties 
            expect(item).toHaveProperty("name");
            expect(item).toHaveProperty("keys");
            expect(Array.isArray(item.keys)).toBe(true);
            expect(item).toHaveProperty("apply_to");
            expect(Array.isArray(item.apply_to)).toBe(true);
            // Verifing structure inside apply_to
            item.apply_to.forEach((applyToItem) => {
                expect(applyToItem).toHaveProperty("name");
                expect(applyToItem).toHaveProperty("calculation_type_id");
            });
        });
        //Verigyin if rest have the properties error and msg 
        expect(res.body).toHaveProperty("error", false);
        expect(res.body).toHaveProperty("msg", "");
    }), 20000);
});
// });
