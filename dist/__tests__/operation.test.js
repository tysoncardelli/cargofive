"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const { Sequelize } = require('sequelize');
const server_1 = __importDefault(require("../models/server"));
const supertest_1 = __importDefault(require("supertest"));
const server = new server_1.default();
const req = (0, supertest_1.default)(server.app);
let sequelize;
beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
    // Configuración de Sequelize para pruebas
    sequelize = new Sequelize('a6dm653k3xrscxcs', 'yq40wwiu7nofyjxn', 'bbdilj0vn6aix4su', {
        host: 'esilxl0nthgloe1y.chr7pe7iynqr.eu-west-1.rds.amazonaws.com',
        dialect: 'mysql',
    });
    // Realiza la conexión antes de todas las pruebas
    yield sequelize.authenticate();
}));
afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
    // Cierra la conexión después de todas las pruebas
    yield sequelize.close();
}));
// describe('set of arithmetic operations', () => {
//     test('sum of numbers', () => {
//         expect(sum(1, 1)).toBe(2);
//     });
describe('getAll', () => {
    it("should return all surcharges", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield req.get("/api/surcharges");
        console.log(res);
        expect(res.status).toBe(200);
    }));
});
// });
