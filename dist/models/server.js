"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ai_routes_1 = __importDefault(require("../routes/ai.routes"));
const crud_routes_1 = __importDefault(require("../routes/crud.routes"));
const cors_1 = __importDefault(require("cors"));
class Server {
    constructor() {
        this.app = (0, express_1.default)();
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.use(express_1.default.urlencoded({ limit: '50mb', extended: true }));
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.static('public'));
        this.port = process.env.PORT || '8001';
        this.getRoutes().then((routes) => {
            this.routes(routes);
        });
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log("Running Server at Port " + this.port);
        });
    }
    routes(routes) {
        for (let i = 0; i < routes.length; i++) {
            if (routes[i] == "/api/ia") {
                this.app.use(routes[i], ai_routes_1.default);
            }
            if (routes[i] == "/api/surcharges" || routes[i] == "/api/carriers" || routes[i] == "/api/rates" || routes[i] == "/api/standard_surcharges" || routes[i] == "/api/calculation_types") {
                this.app.use(routes[i], crud_routes_1.default);
            }
        }
    }
    getRoutes() {
        return __awaiter(this, void 0, void 0, function* () {
            let routes = [
                "/api/ia",
                "/api/surcharges",
                "/api/carriers",
                "/api/rates",
                "/api/standard_surcharges",
                "/api/calculation_types"
            ];
            return routes;
        });
    }
}
exports.default = Server;
