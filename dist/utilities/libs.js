"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.apiResponse = exports.createPrompt = exports.getModelByName = exports.doRequest = exports.getModelName = void 0;
const calculationTypes_model_1 = require("./../models/calculationTypes.model");
var pluralize = require('pluralize');
const surcharges_model_1 = require("./../models/surcharges.model");
const carriers_model_1 = require("./../models/carriers.model");
const rates_model_1 = require("./../models/rates.model");
const standarSurcharges_model_1 = require("../models/standarSurcharges.model");
const getModelName = (req) => __awaiter(void 0, void 0, void 0, function* () {
    const resource = req.originalUrl.split("/")[2];
    console.log(resource);
    let moduleSplitted = resource.split("?")[0];
    let moduleName = "";
    moduleName = pluralize.singular(moduleSplitted);
    return moduleName;
});
exports.getModelName = getModelName;
const doRequest = (options) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise(function (resolve, reject) {
        var request = require('request');
        request(options, function (error, res, body) {
            if (!error && res.statusCode === 200) {
                resolve(body);
            }
            else {
                reject(error);
            }
        });
    });
});
exports.doRequest = doRequest;
const getModelByName = (modelName) => {
    switch (modelName) {
        case 'standard_surcharge':
            return standarSurcharges_model_1.standardSurcharges;
        case 'surcharge':
            return surcharges_model_1.surCharge;
        case 'carrier':
            return carriers_model_1.carrier;
        case 'rate':
            return rates_model_1.rate;
        case 'calculation_type':
            return calculationTypes_model_1.calculationTypes;
        // Agrega más casos según sea necesario para otros modelos
        default:
            return null;
    }
};
exports.getModelByName = getModelByName;
const createPrompt = (surcharges) => __awaiter(void 0, void 0, void 0, function* () {
    let content = `Agrupa y estandariza  los recargos en la siguiente lista, proporcionando nombres estandarizados para cada uno. Considera la posibilidad de agrupar recargos similares y estandarizar los nombres para facilitar su manejo, nombres con palabras origen y destino se agrupan en el mismo grupo: ${surcharges}. Responde en formato json listo para convertir en una aplicacion de nodejs sin ninguna otra explicación: Ejemplo {"grupo1":["element1, "element2"],"group2":["element1, "element2"],}`;
    let messages = [
        { "role": "system", "content": content }
    ];
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + process.env.OPENAI_API_KEY
    };
    let body = {
        model: "gpt-3.5-turbo",
        messages: messages,
    };
    let options = {
        url: `https://api.openai.com/v1/chat/completions`,
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body),
    };
    return options;
});
exports.createPrompt = createPrompt;
const apiResponse = (status, data, error, msg) => __awaiter(void 0, void 0, void 0, function* () {
    return { "statusCode": status, "data": data, "error": error, "msg": msg };
});
exports.apiResponse = apiResponse;
