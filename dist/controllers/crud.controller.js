"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteOne = exports.updateOne = exports.postOne = exports.getOne = exports.getAll = void 0;
const libs_1 = require("../utilities/libs");
const libs_2 = require("../utilities/libs");
const getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let modelName = yield (0, libs_2.getModelName)(req);
    console.log(modelName);
    const Model = yield (0, libs_1.getModelByName)(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model);
    if (!Model) {
        return res.status(404).json(yield (0, libs_1.apiResponse)(400, [], true, 'Model not found'));
    }
    let response = yield Model.findAll();
    res.json(yield (0, libs_1.apiResponse)(200, response ? response : [], false, ""));
});
exports.getAll = getAll;
const getOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let modelName = yield (0, libs_2.getModelName)(req);
    const Model = yield (0, libs_1.getModelByName)(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model);
    if (!Model) {
        return res.status(404).json(yield (0, libs_1.apiResponse)(400, [], true, 'Model not found'));
    }
    let response = yield Model.findOne({ where: { "id": req.params.id } });
    res.json(yield (0, libs_1.apiResponse)(200, response ? response : {}, false, ""));
});
exports.getOne = getOne;
const postOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let modelName = yield (0, libs_2.getModelName)(req);
    const Model = yield (0, libs_1.getModelByName)(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model);
    if (!Model) {
        return res.status(404).json({ error: 'Model not found' });
    }
    let response = yield Model.create(req.body);
    res.json(yield (0, libs_1.apiResponse)(200, response, false, ""));
});
exports.postOne = postOne;
const updateOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let modelName = yield (0, libs_2.getModelName)(req);
    const Model = yield (0, libs_1.getModelByName)(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model);
    if (!Model) {
        return res.status(404).json({ error: 'Model not found' });
    }
    let response = yield Model.update(req.body, { where: { id: req.params.id } });
    let item = yield Model.findOne({ where: { "id": req.params.id } });
    res.json(yield (0, libs_1.apiResponse)(200, item, false, ""));
});
exports.updateOne = updateOne;
const deleteOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let modelName = yield (0, libs_2.getModelName)(req);
    const Model = yield (0, libs_1.getModelByName)(modelName);
    console.log("modelName", modelName);
    console.log("model*", Model);
    if (!Model) {
        return res.status(404).json({ error: 'Model not found' });
    }
    let item = yield Model.findOne({ where: { "id": req.params.id } });
    let response = yield Model.destroy({ where: { id: req.params.id } });
    res.json(yield (0, libs_1.apiResponse)(200, item, false, ""));
});
exports.deleteOne = deleteOne;
