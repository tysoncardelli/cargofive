"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.importData = exports.standarize = void 0;
const libs_1 = require("../utilities/libs");
const libs_2 = require("../utilities/libs");
const standarSurcharges_model_1 = require("../models/standarSurcharges.model");
const XLSX = require('xlsx');
const stringSimilarity = require('string-similarity');
const standarize = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let surchargeModel = yield (0, libs_1.getModelByName)('surcharge');
    let noStandardSurcharges = yield surchargeModel.findAll();
    let categories = [];
    let surcharges = [];
    //transforming data to send in to GPT 
    for (let i = 0; i < noStandardSurcharges.length; i++) {
        surcharges.push(noStandardSurcharges[i].dataValues.name);
        categories.push(noStandardSurcharges[i].dataValues);
    }
    // Creating Object Request to GPT     
    let options = yield (0, libs_2.createPrompt)(surcharges);
    let response = yield (0, libs_2.doRequest)(options);
    //@ts-ignore
    let jsonResponse = JSON.parse(response);
    //Getting Standarize Array Response 
    let standardSurcharges = JSON.parse(jsonResponse.choices[0].message.content);
    const groups = [];
    //Changing  Structure of the Data 
    for (const groupName in standardSurcharges) {
        const keys = standardSurcharges[groupName];
        groups.push({ name: groupName, keys });
    }
    //Triming original array for comparing 
    categories = categories.map((surcharge) => {
        surcharge.name = surcharge.name.trim();
        return surcharge;
    });
    //Getting final array to Store 
    const result = groups.map(group => {
        const apply_to = [];
        for (const surcharge of categories) {
            if (group.keys.includes(surcharge.name)) {
                // Verifyin if the arary already exists in apply to
                const existingApplyTo = apply_to.find((item) => item.name === surcharge.apply_to);
                if (!existingApplyTo) {
                    apply_to.push({
                        name: surcharge.apply_to,
                        calculation_type_id: surcharge.calculation_type_id
                    });
                }
            }
        }
        return {
            name: group.name,
            keys: group.keys,
            apply_to: apply_to
        };
    });
    //Inserting in standard surcharges
    console.log(req.query.insert);
    if (req.query.insert == 'true') {
        let Model = yield (0, libs_1.getModelByName)('standard_surcharge');
        for (let i = 0; i < result.length; i++)
            yield Model.create(result[i]);
    }
    res.json(yield (0, libs_1.apiResponse)(200, result, false, ""));
});
exports.standarize = standarize;
const importData = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let standardData = yield standarSurcharges_model_1.standardSurcharges.findAll();
    // transforming array 
    let keyObjectArray = [];
    for (let i = 0; i < standardData.length; i++) {
        let object = {
            "id": standardData[i].dataValues.id,
            "keys": []
        };
        //@ts-ignore
        object.keys.push(standardData[i].dataValues.name);
        //@ts-ignore
        object.keys.push(...standardData[i].dataValues.keys);
        keyObjectArray.push(object);
    }
    try {
        // Accessing file data 
        const buffer = req.file.buffer;
        // converting excel content
        const workbook = XLSX.read(buffer, { type: 'buffer' });
        // getting first sheet
        const sheetName = workbook.SheetNames[0];
        const sheet = workbook.Sheets[sheetName];
        // converting to json array
        const jsonData = XLSX.utils.sheet_to_json(sheet);
        // Aditional logic to get and insert rates
        const mostSimilarIds = yield findMostSimilarSurcharge(keyObjectArray, jsonData);
        res.json(yield (0, libs_1.apiResponse)(200, jsonData, false, ""));
    }
    catch (error) {
        console.error(error);
        res.status(500).send('Error processing the file');
    }
});
exports.importData = importData;
//@ts-ignore
function findMostSimilarSurcharge(keys, surcharges) {
    return __awaiter(this, void 0, void 0, function* () {
        const result = [];
        surcharges.forEach((surcharge) => __awaiter(this, void 0, void 0, function* () {
            console.log(surcharge.Surcharge);
            let probabilities = [];
            //@ts-ignore
            keys.forEach(({ id, keys: keywords }) => {
                let totalSimilarity = 0;
                keywords.forEach((keyWord) => {
                    let similarity = stringSimilarity.compareTwoStrings(surcharge.Surcharge.toLowerCase(), keyWord.toLowerCase());
                    totalSimilarity = totalSimilarity + similarity;
                });
                totalSimilarity = totalSimilarity / keywords.length;
                probabilities.push({ "id": id, "probability": totalSimilarity });
            });
            let selected = { "id": 0, "probability": 0 };
            probabilities.forEach((item) => {
                if (item.probability > selected.probability) {
                    selected.id = item.id;
                    selected.probability = item.probability;
                }
            });
            //console.log(selected)
            let CarrierModel = (0, libs_1.getModelByName)("carrier");
            let rateModel = (0, libs_1.getModelByName)("rate");
            let carrier = yield CarrierModel.findOne({ where: { name: surcharge.Carrier } });
            //console.log(carrier)
            let resultObject = {
                "carrier_id": carrier.dataValues.id,
                "surcharge_id": selected.id,
                "amount": surcharge.Amount,
                "currency": surcharge.Currency,
                "apply_to": surcharge["Apply to"]
            };
            yield rateModel.create(resultObject);
        }));
        return result;
    });
}
