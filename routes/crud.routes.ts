import {Router} from 'express'
import { deleteOne, getAll, getOne, postOne, updateOne } from '../controllers/crud.controller';

const router = Router();

router.get('/', getAll)
router.get('/:id', getOne)
router.post('/', postOne)
router.put('/:id', updateOne)
router.delete('/:id', deleteOne)


export default router;