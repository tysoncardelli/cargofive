import {Router} from 'express'
import {importData, standarize} from '../controllers/ia.controller'
import multer from 'multer';
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });


const router = Router();

router.get('/standarize', standarize)
router.post('/import', upload.single('excelFile'), importData)


export default router;