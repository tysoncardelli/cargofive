import express, {Application} from 'express';
import iaRoutes from '../routes/ai.routes'
import crudRoutes from '../routes/crud.routes'
import cors from 'cors'


class Server {
    public  app: Application;
    private port: string

    constructor(){
        this.app = express();
        this.app.use(express.json({limit: '50mb'}))
        this.app.use(express.urlencoded({limit: '50mb', extended: true}))
        this.app.use(cors());
        this.app.use(express.static('public'));

        this.port = process.env.PORT || '8001';
        this.getRoutes().then((routes) =>{
            this.routes(routes)
        })
    }

    listen(){
        this.app.listen(this.port, ()=> {
            console.log("Running Server at Port " + this.port)
        })
    }

    routes(routes: any){

        for(let i = 0; i < routes.length; i++){
            if(routes[i] =="/api/ia"){
                this.app.use(routes[i], iaRoutes)
            }
            if(routes[i] =="/api/surcharges" || routes[i] =="/api/carriers" || routes[i] =="/api/rates" || routes[i] =="/api/standard_surcharges" || routes[i] =="/api/calculation_types"){
                this.app.use(routes[i], crudRoutes)
            }
        }
    }

    async getRoutes(){
        let routes = [
            "/api/ia",
            "/api/surcharges",
            "/api/carriers",
            "/api/rates",
            "/api/standard_surcharges",
            "/api/calculation_types"
        ]
        return routes;
    }

}
export default Server;