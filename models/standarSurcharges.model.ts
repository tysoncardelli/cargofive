import * as Sequelize from 'sequelize'
const {sequelize} = require("../datasources/sequelize")


export const standardSurcharges = sequelize.define('standard_surcharge', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    apply_to: {
      type: Sequelize.JSON,
    }, 
    keys: {
      type: Sequelize.JSON,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    }
  }, {
       createdAt: 'created_at',
       updatedAt: 'updated_at'
  });

