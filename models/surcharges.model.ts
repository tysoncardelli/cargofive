import * as Sequelize from 'sequelize'
const {sequelize} = require("../datasources/sequelize")


export const surCharge = sequelize.define('surcharge', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    apply_to: {
      type: Sequelize.STRING,
    }, 
    calculation_type_id: {
      type: Sequelize.INTEGER,
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    }
  }, {
    createdAt: false,
    updatedAt: false
  });

