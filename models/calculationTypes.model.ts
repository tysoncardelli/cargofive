import * as Sequelize from 'sequelize'
const {sequelize} = require("../datasources/sequelize")


export const calculationTypes = sequelize.define('calculation_type', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    createdAt: false,
    updatedAt: false
  });

