import * as Sequelize from 'sequelize'
const {sequelize} = require("../datasources/sequelize")


export const carrier = sequelize.define('carrier', {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    createdAt: false,
    updatedAt: false
  });

