import * as Sequelize from 'sequelize'
const {sequelize} = require("../datasources/sequelize")


export const rate = sequelize.define('rate', {
    surcharge_id: {
      type: Sequelize.BIGINT,
    },
    carrier_id: {
      type: Sequelize.BIGINT,
    }, 
    amount: {
      type: Sequelize.FLOAT,
    },
    currency: {
      type: Sequelize.STRING,
    },
    apply_to: {
      type: Sequelize.STRING,
    },
    created_at: {
        type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE,
    }
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });

